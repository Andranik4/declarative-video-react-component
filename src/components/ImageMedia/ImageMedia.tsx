import {
    useEffect,
    useRef,
    useState,
    CanvasHTMLAttributes,
} from "react";
import useRaf from "@rooks/use-raf";
import { TMediaState } from 'types'

export interface IImageMediaProps extends Omit<
    React.DetailedHTMLProps<CanvasHTMLAttributes<HTMLCanvasElement>, HTMLCanvasElement>
    , 'src'> {
    currentTime: number;
    duration: number;
    play?: boolean;
    src?: string;
    delta?: number;
    onCurrentTimeUpdate?: (currentTime: number) => void
    onMediaStateUpdate?: (err: any, videoState: TMediaState) => void
}

function ImageMedia(props: IImageMediaProps) {
    const {
        currentTime,
        duration,
        play = false,
        delta = 0,
        src,
        onCurrentTimeUpdate,
        onMediaStateUpdate,
        ...restProps
    } = props
    const url = src ?? ''

    const imgRef = useRef<HTMLImageElement>(new Image())
    const canvasRef = useRef<HTMLCanvasElement>(null)
    const lastTimeRef = useRef<number | null>(null)
    const imgMetaRef = useRef({
        _currentTime: 0,
        get currentTime() { return this._currentTime },
        set currentTime(value: number) {
            if (value < 0) {
                this._currentTime = 0;
            } else if (value > duration) {
                this._currentTime = duration
            } else {
                this._currentTime = value
            }
        },
        ended: false,
    })

    const [localState, setLocalState] = useState({
        localCurrentTime: currentTime,
        state: 'idle' as TMediaState,
        loading: true,
    })
    const {
        localCurrentTime,
        loading,
    } = localState

    const sources = !!url ? [url] : []

    const errorHandler = () => {
        if (!imgRef.current) return
        const { current: imgMeta } = imgMetaRef
        imgMeta.currentTime = 0
        imgMeta.ended = false
        setLocalState({
            localCurrentTime: imgMeta.currentTime,
            state: !url ? 'idle' : 'corrupted',
            loading: false,
        })
    }

    const finishHandler = () => {
        setLocalState((state) => ({
            ...state,
            state: 'finish',
        }))
    }

    const loadedhandler = () => {
        if (!imgRef.current) return
        const { current: imgMeta } = imgMetaRef
        const { current: img } = imgRef
        const { current: canvas } = canvasRef
        if (canvas) {
            const ctx = canvas.getContext('2d')
            if (ctx) {
                ctx.drawImage(img, 0, 0, canvas.width, canvas.height)
            }
        }
        imgMeta.currentTime = 0
        imgMeta.ended = false
        setLocalState({
            localCurrentTime: imgMeta.currentTime,
            state: 'ready',
            loading: false,
        })
        imgMeta.currentTime += currentTime
        imgMeta.ended = imgMeta.currentTime === duration
        if (imgMeta.ended) finishHandler()
        // imgMeta.currentTime = 0
        imgMeta.ended = false
    }

    useEffect(() => {
        if (localState.state) {
            props.onMediaStateUpdate?.(null, localState.state)
        }
    }, [localState.state])

    useEffect(() => {
        if (!imgRef.current) return
        const { current: imgElem } = imgRef
        imgElem.addEventListener('load', loadedhandler)
        imgElem.addEventListener('error', errorHandler)
        return () => void clearImgHandlers();

        function clearImgHandlers() {
            imgElem.removeEventListener('load', loadedhandler)
            imgElem.removeEventListener('error', errorHandler)
        }
    })

    useEffect(() => {
        if (!imgRef.current) return
        const { current: img } = imgRef
        img.src = url
        setLocalState((state) => ({
            ...state,
            loading: true,
            state: sources.length ? 'loading' : state.state,
            // localCurrentTime: localCurrentTime,
        }))
        return () => void (imgRef.current = new Image())
    }, [src])

    useEffect(() => {
        if (!imgRef.current) return
        return () => {
            const { current: imgMeta } = imgMetaRef
            imgMeta.currentTime = currentTime
        }
    }, [loading])

    useEffect(() => {
        const { current: imgMeta } = imgMetaRef

        lastTimeRef.current = currentTime
        if (!imgRef.current) return
        imgMeta.currentTime = currentTime
        imgMeta.ended = imgMeta.currentTime === duration
        if (imgMeta.ended) {
            finishHandler()
        } else if (Math.abs(currentTime - localCurrentTime) > delta) {
            if (localState.state !== 'corrupted' && localState.state !== 'idle')
                setLocalState((state) => ({
                    ...state,
                    state: 'ready',
                }))
        }
    }, [currentTime])

    useEffect(() => {
        return () => {
            if (!imgRef.current) return
            const imgMeta = imgMetaRef.current

            const lastTime = lastTimeRef.current
            if (lastTime === null) return
            if (Math.abs(lastTime - localCurrentTime) > delta) {
                imgMeta.currentTime = lastTime;
            }
        }
    }, [localCurrentTime])

    const shouldAnimate = play && (!loading) && (localState.state === 'ready')

    useRaf((elapsed) => {
        const { current: imgMeta } = imgMetaRef
        if (!imgRef.current) return
        if (!play || loading || imgMeta.ended) return
        const elapsedSeconds = elapsed / 1000
        imgMeta.ended = imgMeta.currentTime === duration
        if (imgMeta.ended) {
            props.onCurrentTimeUpdate?.(imgMeta.currentTime)
            finishHandler()
        } else {
            imgMeta.currentTime += elapsedSeconds
            imgMeta.ended = imgMeta.currentTime === duration
            setLocalState((prevState) => {
                return ({
                    ...prevState,
                    localCurrentTime: imgMeta.currentTime,
                })
            })
            props.onCurrentTimeUpdate?.(imgMeta.currentTime)
        }
    }, shouldAnimate)

    return (
        <canvas
            ref={canvasRef}
            key={`${url}`}
            {...restProps}
        />
    )
}

export default ImageMedia