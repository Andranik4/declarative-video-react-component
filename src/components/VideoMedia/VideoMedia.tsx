import {
    useEffect,
    useRef,
    useState,
    VideoHTMLAttributes,
} from "react";
import useRaf from "@rooks/use-raf";
import { TMediaState } from 'types'

export interface IVideoProps extends Omit<
    React.DetailedHTMLProps<VideoHTMLAttributes<HTMLVideoElement>, HTMLVideoElement>
    , 'src'> {
    currentTime: number;
    play?: boolean;
    muted?: boolean;
    src?: string | string[];
    delta?: number;
    onDurationUpdate?: (duration: number) => void
    onCurrentTimeUpdate?: (currentTime: number) => void
    onMediaStateUpdate?: (err: any, videoState: TMediaState) => void
}

function Video(props: IVideoProps) {
    const videoRef = useRef<HTMLVideoElement>(null)
    const lastSourceRef = useRef<HTMLSourceElement>(null)
    const lastTimeRef = useRef<number | null>(null)

    const {
        currentTime,
        play = false,
        muted = false,
        delta = 0,
        src,
        onDurationUpdate,
        onCurrentTimeUpdate,
        onMediaStateUpdate,
        ...restProps
    } = props
    const [localState, setLocalState] = useState({
        localCurrentTime: currentTime,
        state: 'idle' as TMediaState,
        loading: true,
    })
    const {
        localCurrentTime,
        loading,
    } = localState

    const sources = src instanceof Array ? src : (!!src ? [src] : [])

    const durationChangeHandler = () => {
        if (!videoRef.current) return
        const { current: videoElem } = videoRef
        props.onDurationUpdate?.(videoElem.duration)
    }

    const errorHandler = () => {
        if (!videoRef.current) return
        const { current: videoElem } = videoRef
        props.onDurationUpdate?.(videoElem.duration)
        setLocalState({
            localCurrentTime: videoElem.currentTime,
            state: 'corrupted',
            loading: false,
        })
    }

    const finishHandler = () => {
        setLocalState((state) => ({
            ...state,
            state: 'finish',
        }))
    }

    const emptiedHandler = () => {
        if (!videoRef.current) return
        const { current: videoElem } = videoRef

        if (!sources.length) {
            props.onDurationUpdate?.(videoElem.duration)
            setLocalState({
                localCurrentTime: videoElem.currentTime,
                state: 'idle',
                loading: false,
            })
        }
    }

    const loadedhandler = () => {
        if (!videoRef.current) return
        const { current: videoElem } = videoRef

        setLocalState({
            localCurrentTime: videoElem.currentTime,
            state: 'ready',
            loading: false,
        })
    }

    useEffect(() => {
        if (!videoRef.current) return
        const { current: videoElem } = videoRef
        props.onDurationUpdate?.(videoElem.duration)
    }, [])
    
    useEffect(() => {
        if (localState.state) {
            props.onMediaStateUpdate?.(null, localState.state)
        }
    }, [localState.state])

    useEffect(() => {
        if (!videoRef.current) return
        const { current: videoElem } = videoRef
        videoElem.addEventListener('durationchange', durationChangeHandler)
        videoElem.addEventListener('loadedmetadata', loadedhandler)
        videoElem.addEventListener('ended', finishHandler)
        videoElem.addEventListener('emptied', emptiedHandler)
        videoElem.addEventListener('error', errorHandler)
        if (!lastSourceRef.current) return () => void clearVideoHandlers();
        const { current: lastSourceElem } = lastSourceRef
        lastSourceElem.addEventListener('error', errorHandler)
        return () => (void clearVideoHandlers(), void clearSourceHandlers());

        function clearVideoHandlers() {
            videoElem.removeEventListener('durationchange', durationChangeHandler)
            videoElem.removeEventListener('loadedmetadata', loadedhandler)
            videoElem.removeEventListener('ended', finishHandler)
            videoElem.removeEventListener('emptied', emptiedHandler)
            videoElem.removeEventListener('error', errorHandler)
        }

        function clearSourceHandlers() {
            lastSourceElem.removeEventListener('error', errorHandler)
        }
    })

    useEffect(() => {
        if (!videoRef.current) return
        const { current: videoElem } = videoRef
        if (sources.length) {
        }
        setLocalState((state) => ({
            loading: true,
            state: sources.length ? 'loading' : state.state,
            localCurrentTime: videoElem.currentTime,
        }))
    }, [src])

    useEffect(() => {
        if (!videoRef.current) return
        const { current: videoElem } = videoRef
        if (loading) {
            videoElem.load()
        }
        return () => {
            videoElem.currentTime = currentTime
        }
    }, [loading])

    useEffect(() => {
        if (!videoRef.current) return
        const { current: videoElem } = videoRef
        if (loading || videoElem.ended /* || videoElem.readyState < 2 */) return
        if (play && videoElem.paused) {
            videoElem.play()
        } else if (!play && !videoElem.paused) {
            videoElem.pause()
        }
    })

    useEffect(() => {
        if (!videoRef.current) return
        const { current: videoElem } = videoRef
        videoElem.muted = muted
    }, [muted])

    useEffect(() => {
        lastTimeRef.current = currentTime
        if (!videoRef.current) return
        const { current: videoElem } = videoRef
        if (Math.abs(currentTime - localCurrentTime) > delta) {
            if (videoElem.ended) {
                setLocalState((state) => ({
                    ...state,
                    state: 'ready',
                }))
            }
            videoElem.currentTime = currentTime
        }
    }, [currentTime])

    useEffect(() => {
        return () => {
            if (!videoRef.current) return
            const { current: videoElem } = videoRef

            const lastTime = lastTimeRef.current
            if (lastTime === null) return
            if (Math.abs(lastTime - localCurrentTime) > delta) {
                videoElem.currentTime = lastTime
            }
        }
    }, [localCurrentTime])

    const shouldAnimate = play && (!loading) && (localState.state === 'ready')

    useRaf(() => {
        if (!videoRef.current) return
        const { current: videoElem } = videoRef

        if (!play || loading || videoElem.ended) return

        setLocalState((prevState) => {
            return ({
                ...prevState,
                localCurrentTime: videoElem.currentTime,
            })
        })
        props.onCurrentTimeUpdate?.(videoElem.currentTime)
    }, shouldAnimate)

    return (
        <video ref={videoRef} {...restProps}>
            {sources.map((source, index) => (
                <source key={source} src={source} ref={index === sources.length - 1 ? lastSourceRef : null} />
            ))}
        </video>
    )
}

export default Video
