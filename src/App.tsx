import React, { useMemo, useState } from 'react'
import VideoMedia from 'components/VideoMedia/VideoMedia'
import ImageMedia from 'components/ImageMedia/ImageMedia'
import videoFiles from 'resources/videos.json'

interface IVideoTrack {
  id: string;
  typeId: string;
};

interface IVideoFile {
  id: string;
  url: string;
  description?: string;
  subtitle?: string;
  thumb?: string;
  title?: string;
};

function App() {
  const [timeline, setTimeline] = useState<IVideoTrack[]>([]);
  const [player, setPlayer] = useState({
    isPlaying: false,
    currentTime: 0,
    currentIndex: 0,
  })
  const [draftCurrentTime, setDraftCurrentTime] = useState(0)
  const [hasTimeHandler, setHasTimeHandler] = useState(true)
  const [source, setSource] = useState<any>('')

  const trackToFileMap = useMemo(
    () => timeline.reduce((map, track) => {
      const videoFile = videoFiles.find((videoFile) => videoFile.id === track.typeId)!
      return map.set(track, videoFile as any)
    }, new Map<IVideoTrack, IVideoFile>()), [timeline])

  const toggleVideoTrack = (videoFile: IVideoFile) => {
    setTimeline((state) => {
      const filtered = state.filter((videoTrack: any) => videoTrack.typeId !== videoFile.id)
      if (filtered.length === state.length) {
        filtered.push({
          id: `${Math.random()}`,
          typeId: videoFile.id,
        })
      }
      return filtered;
    })
  }

  let url = '';
  if (player.currentIndex <= timeline.length - 1) {
    url = trackToFileMap.get(timeline[player.currentIndex])!.url
  }
  // console.log(typeof url)
  // console.log(url)
  return (
    <div className="App">
      <h1>Custom VideoMedia</h1>
      {/* <VideoMedia
        currentTime={player.currentTime}
        play={player.isPlaying}
        src={url}
        onCurrentTimeUpdate={hasTimeHandler && ((currentTime: number) => {
          setPlayer((state) => ({ ...state, currentTime, }))
        }) as any}
        onDurationUpdate={(duration) => console.log('duration', duration)}
        onMediaStateUpdate={(err, mediaState) => console.log('media state', mediaState)}
        width={500}
        height={400}
        crossOrigin=""
      /> */}
      <ImageMedia
        currentTime={player.currentTime}
        play={player.isPlaying}
        src={url}
        duration={10}
        // delta={0.4}
        onCurrentTimeUpdate={hasTimeHandler && ((currentTime: number) => {
          setPlayer((state) => ({ ...state, currentTime, }))
        }) as any}
        onMediaStateUpdate={(err, mediaState) => console.log('media state', mediaState)}
        width={500}
        height={400}
      ></ImageMedia>
      <div>
        <h2>Media Current Time {player.currentTime} seconds</h2>
        <button onClick={() => setPlayer((state) => ({ ...state, isPlaying: !state.isPlaying }))}>{player.isPlaying ? 'Pause' : 'Play'}</button>
        <div>
          <input type="text" value={draftCurrentTime} onChange={(e) => setDraftCurrentTime(+e.target.value)} />
          <button onClick={() => setPlayer((state) => ({ ...state, currentTime: draftCurrentTime }))}>Change CurrentTime</button>
        </div>
        <button onClick={() => setHasTimeHandler((state) => !state)}>Add time handler</button>
        <section>
          <h3>Media Files</h3>
          <ul className="VideoFilesList">
            {videoFiles.map((videoFile) => (
              <li key={`${videoFile.id}`}>
                <div
                  className={'VideoFile' + (timeline.find((track) => track.typeId === videoFile.id) ? ' AddedToTimeline' : '')}
                  onClick={() => toggleVideoTrack(videoFile as any)}
                >
                  <div className="VideoFileTitleSection">
                    <p>{videoFile.title}</p>
                    <img src={videoFile.thumb} alt={videoFile.title} />
                  </div>
                  <p className="VideoFileDescription">{videoFile.description}</p>
                </div>
              </li>
            ))}
          </ul>
        </section>
      </div>
    </div>
  );
}

export default App;
