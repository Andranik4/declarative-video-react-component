export type TMediaState = 'idle' | 'loading' | 'ready' | 'finish' | 'corrupted'
